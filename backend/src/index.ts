import express, { Request, Response } from 'express'
import { getLanguages } from './db'

const server = express()

server.use('/', express.static('./dist/client'))

server.get('/version', (req: Request, res: Response) => {
    res.send('Server version 2.0')
})

server.get('/languages', async (req: Request, res: Response) => {
    const languages = await getLanguages()
    res.send(languages)
})

const PORT = process.env.PORT
server.listen(PORT, () => console.log('Listening', PORT))