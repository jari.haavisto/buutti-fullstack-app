import { useEffect, useState } from "react"

interface Language {
    id: number
    name: string
}

function App() {
    const [languages, setLanguages] = useState<Array<Language>>([])

    useEffect(() => {
        getLanguages()
    }, [])

    const getLanguages = async () => {
        const response = await fetch('/languages')
        const result = await response.json()
        setLanguages(result)
    }

    return (
        <div className='App'>
            {languages.map(language => {
                return <div key={language.id}>{language.name}</div>
            })}
        </div>
    )
}

export default App
